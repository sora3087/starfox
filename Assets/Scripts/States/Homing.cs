﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : ArwingState {
	GameObject HomingScan;
    GameObject ReticleFar;
    SpriteRenderer ReticleNearSprite;
    SpriteRenderer ReticleFarSprite;
    Vector3 ReticleDefaultScale;

    float ReticleTimer;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (!HomingScan)
        	HomingScan = arwingObject.transform.Find("homingScan").gameObject;
        if (!ReticleFar)
            ReticleFar = arwing.reticleFar;
        if (!ReticleNearSprite)
            ReticleNearSprite = arwing.reticleNearSprite;
        if (!ReticleFarSprite)
            ReticleFarSprite = arwing.reticleFarSprite;
        if (ReticleDefaultScale == Vector3.zero)
            ReticleDefaultScale = ReticleFar.transform.localScale;

        ReticleNearSprite.color = arwing.ReticalNearChargeColor;
        ReticleFarSprite.color = arwing.ReticalFarChargeColor;

        ReticleTimer = 0.0f;
        HomingScan.SetActive(true);
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ReticleTimer += Time.deltaTime*2f;
        if (ReticleTimer >= 1.0f)
            ReticleTimer = 0.0f;
        ReticleFar.transform.localScale = ReticleDefaultScale * (1.0f + (Easing.easeInQuad(ReticleTimer) * 0.5f));
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        HomingScan.SetActive(false);
        ReticleNearSprite.color = arwing.ReticalDefaultColor;
        ReticleFarSprite.color = arwing.ReticalDefaultColor;
        ReticleFar.transform.localScale = ReticleDefaultScale;
    }
}
