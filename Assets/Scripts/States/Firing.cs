﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firing : ArwingState {
	float burstDelayTimer;
	int burstShotsFired = 0;
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    	burstShotsFired = 1;
        animator.SetInteger("burstShotsFired", burstShotsFired);
        burstDelayTimer = arwing.burstDelay;
        arwing.FireLaser();
	}
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        burstDelayTimer -= Time.deltaTime;
        if (burstDelayTimer <= 0f)
        {
        	burstShotsFired++;
	        animator.SetInteger("burstShotsFired", burstShotsFired);
            burstDelayTimer = arwing.burstDelay;
            arwing.FireLaser();
        }
	}
}
