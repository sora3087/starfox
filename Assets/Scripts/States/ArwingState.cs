using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArwingState : StateMachineBehaviour {
	public GameObject arwingObject;
	public Arwing arwing;
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (!arwingObject) 
		    arwingObject = animator.gameObject;
        if (!arwing)
		    arwing = arwingObject.GetComponent<Arwing>();
	}
}
