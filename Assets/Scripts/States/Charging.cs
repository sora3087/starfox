﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charging : ArwingState {
	GameObject ChargeEffect;
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (!ChargeEffect)
        	ChargeEffect = arwingObject.transform.Find("ChargeEffect").gameObject;
        ChargeEffect.SetActive(true);
    }
}