﻿public class Axis
{
    public string name { get; protected set; }
    public float value { get; set; }
    public Axis(string _name)
    {
        name = _name;
    }
}
