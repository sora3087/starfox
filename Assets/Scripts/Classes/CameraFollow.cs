﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    GameObject arwing;
    GameController gamepad;
    public float maxHorizontal = 6f;
    public float maxVertical = 3f;
    public float minHorizontal = -6f;
    public float minVertical = -3f;
    public float cameraSpeed = 10f;

    float x;
    float y;

    // Use this for initialization
    void Start () {
        arwing = transform.parent.Find("arwing").gameObject;
        PlayerRig rig = transform.parent.GetComponent<PlayerRig>();
        gamepad = rig.gamepad;
	}
	
	// Update is called once per frame
	void Update () {
        x = Mathf.Clamp(x - gamepad.GetAxis(0)*Time.deltaTime*cameraSpeed, minHorizontal, maxHorizontal);
        y = Mathf.Clamp(y - gamepad.GetAxis(1)*Time.deltaTime*cameraSpeed, minVertical, maxVertical);

        transform.localPosition = new Vector3(arwing.transform.localPosition.x+x, arwing.transform.localPosition.y+y, transform.localPosition.z);
    }
}
