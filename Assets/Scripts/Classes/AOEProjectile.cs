﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AOEProjectile : Projectile {
    //editor values
    [Space(5)]
    [Header("Inherited from AOEProjectile class")]
    public float effectRadius = 5.0f;
    public int effectDamagePerTick = 1;
    public float effectTime = 2/60;//one logic frame
    public GameObject areaPrefab;

    //timers
    float effectAliveTime = 0;

    //states
    bool effectActive = false;

    //private variables
    GameObject area;
    List<Ship> areaCollidedObjects;


    void Start() {
        areaCollidedObjects = new List<Ship>();
    }

    void createArea() {
        //create an area of affect that does damage per update until despawn
        area = Instantiate(areaPrefab, transform);
        area.transform.localScale = Vector3.one * (effectRadius * 2);
        effectActive = true;
    }

    void OnTriggerEnter(Collider collider) {
        //bullet collided with target
        createArea();
    }

    public void OnAreaEnter(Collider collider) {
        //event handled from area class
        Ship target = collider.gameObject.GetComponent<Ship>();
        if (target != null) {
            areaCollidedObjects.Add(target);
        }
    }

    public void OnAreaTick() {
        if (effectActive) {
            if (areaCollidedObjects.Count > 0)  {
                foreach (Ship target in areaCollidedObjects)
                {
                    target.Hit(effectDamagePerTick);
                }
            }
            effectAliveTime += Time.deltaTime;
            if (effectAliveTime >= effectTime) {
                Destroy(area);
                Destroy(gameObject);
            }
        }
    }
}
