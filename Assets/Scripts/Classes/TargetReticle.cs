﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetReticle : MonoBehaviour {
    float animationLength = 0.66f;
    public float animationTimer = 0.0f;
    float reticleScale = 0.0f;
    Ship parent;
	// Use this for initialization
	void Start () {
        parent = gameObject.GetComponentInParent<Ship>();
        reticleScale = transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
        animationTimer += Time.deltaTime;
        animationTimer = Mathf.Clamp(animationTimer, 0, animationLength);
        float t = (1f - Easing.easeInOutQuart(animationTimer / animationLength));
        transform.localScale = Vector3.one * (reticleScale + (2f * t));
        transform.eulerAngles = new Vector3(0,0,270f*t);
	}

    //TODO
    // add event listener to parent on hit to remove this reticle if the target was hit but not destroyed
}
