﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour {
    AOEProjectile parent;

    private void Start() {
        parent = gameObject.GetComponentInParent<AOEProjectile>();
    }

    void OnTriggerEnter(Collider collider) {
        parent.OnAreaEnter(collider);
    }

    private void Update() {
        parent.OnAreaTick();
    }
}
