﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingScan : MonoBehaviour {
    [Header("This class sends back its collision to the parent ship Game Object")]
    Arwing ParentScript;
    
    void Start() {
        ParentScript = gameObject.GetComponentInParent<Arwing>();
    }

    void OnTriggerEnter(Collider collider) {
        ParentScript.OnHomingEnter(collider);
    }
}
