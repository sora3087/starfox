﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRig : MonoBehaviour {
    public GameController gamepad;
    // Update is called once per frame
    public void Start()
    {
        gamepad = new GameController(0);
        return;
    }
    void FixedUpdate () {
        gamepad.Update();
        Vector3 newPosition = transform.position;
        newPosition.z += Time.deltaTime * 20;
        transform.position = newPosition;
	}
    public void GetController(int id)
    {
        gamepad = new GameController(id);
    }
}
