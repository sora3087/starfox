﻿using System.Collections.Generic;
using UnityEngine;

public class GameController {

    int idx = 0;
    List<Button> buttons = new List<Button>();
    List<Axis> axes = new List<Axis>();

    public GameController(int id)
    {
        idx = id;
        buttons.Add(new Button(idx.ToString() + "Fire"));
        buttons.Add(new Button(idx.ToString() + "Alt"));
        buttons.Add(new Button(idx.ToString() + "L"));
        buttons.Add(new Button(idx.ToString() + "R"));
        buttons.Add(new Button(idx.ToString() + "Start"));
        axes.Add(new Axis(idx.ToString() + "X"));
        axes.Add(new Axis(idx.ToString() + "Y"));
    }

    public void Update()
    {
        foreach (var button in buttons)
        {
            button.pressed = Input.GetButton(button.name);
        }
        foreach (var axis in axes)
        {
            axis.value = Input.GetAxis(axis.name);
        }
    }
    public bool GetButton(int id)
    {
        return buttons[id].pressed;
    }
    public float GetAxis(int id)
    {
        return axes[id].value;
    }
}
