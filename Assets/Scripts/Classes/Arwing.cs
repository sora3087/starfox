﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arwing : Ship {
    //editor settings
    
    [Header("Motion variables")]
    public float maxRoll = 30.0F;
    public float minRoll = -30.0F;
    public float maxTilt = 30.0F;
    public float minTilt = -30.0F;
    public float maxYaw = -30.0F;
    public float minYaw = 30.0F;
    public float maxRollChange = 0.06f;
    public float maxYawChange = 0.06f;
    public float maxTiltChange = 0.07f;
    public float maxHorizontal = 8.0f;
    public float maxVertical = 4.5f;
    public float minHorizontal = -8.0f;
    public float minVertical = -4.5f;
    public float travel = 0.15f;
    public float bankRate = 1.5f;
    [Space(5)]
    [Header("Shooting variables")]
    [Tooltip("Time, in seconds, the button needs to be held to start homing state")]
    public float homingTime = 1.0f;
    [Tooltip("Number of shots fired in burst state")]
    public int burstShots = 3;
    [Tooltip("Time, in seconds, between burst shots")]
    public float burstDelay = 0.12f;
    public GameObject BulletPrefab;
    public GameObject LockOnPrefab;
    public GameObject HomingPrefab;
    public Color ReticalDefaultColor;
    public Color ReticalNearChargeColor;
    public Color ReticalFarChargeColor;

    //timers
    [Space(5)]
    [Header("Shooting states")]
    float tx;
    float ty;

    //inputs
    public float ix;
    public float iy;
    public bool abutton;
    public bool bbutton;
    public bool sl;
    public bool sr;

    //states
    [Space(5)]
    [Header("Shooting states")]
    [SerializeField]
    float bankLeft = 0f;
    float bankRight = 0f;
    [SerializeField]
    bool bankingLeft = false;
    bool bankingRight = false;

    //buffered references
    public GameObject reticleNear;
    public GameObject reticleFar;
    public SpriteRenderer reticleNearSprite;
    public SpriteRenderer reticleFarSprite;

    GameController gamepad;

    GameObject blasterL;
    GameObject blasterR;
    GameObject blasterC;
    GameObject homingScan;

    GameObject HomingTarget;

    Vector3 OriginalReticleScale;
    float reticleNearDistance;

    Animator StateManager;
    
    void Start() {
        health = 100;
        tx = 0.0f;
        ty = 0.0f;
        //get reticle objects to control
        reticleNear = transform.Find("reticleNear").gameObject;
        reticleNearDistance = reticleNear.transform.localPosition.z;
        reticleFar = transform.Find("reticleFar").gameObject;
        reticleNearSprite = reticleNear.GetComponent<SpriteRenderer>();
        reticleFarSprite = reticleFar.GetComponent<SpriteRenderer>();

        //change reticle colors to default color
        reticleNearSprite.color = ReticalDefaultColor;
        reticleFarSprite.color = ReticalDefaultColor;

        blasterL = transform.Find("blasterL").gameObject;
        blasterR = transform.Find("blasterR").gameObject;
        blasterC = transform.Find("blasterC").gameObject;
        homingScan = transform.Find("homingScan").gameObject;

        StateManager = GetComponent<Animator>();

        gamepad = transform.parent.GetComponent<PlayerRig>().gamepad;
    }

    void Update() {

        tx = Mathf.Clamp(tx - (ix * 0.25f), -2.0f, 2.0f);
        ty = Mathf.Clamp(ty - (iy * 0.25f), -2.0f, 2.0f);

        float roll = Mathf.Lerp(minRoll, maxRoll, (tx + 2) / 4);
        if (bankLeft != 0) {
            roll += Mathf.Lerp(0, 55, Easing.easeInOutQuad(bankLeft));
        }else if (bankRight != 0) {
            roll -= Mathf.Lerp(0, 55, Easing.easeInOutQuad(bankRight));
        }
        Vector3 shipRotation = new Vector3(Mathf.Lerp(minTilt, maxTilt, (ty + 2) / 4), Mathf.Lerp(minYaw, maxYaw, (tx + 2) / 4), roll);
        transform.localEulerAngles = shipRotation;

        transform.localPosition = new Vector3(transform.localPosition.x - tx * travel, transform.localPosition.y - ty*travel, 0);

        Vector3 reticleRotation = new Vector3(0, 0, roll*-1);

        reticleNear.transform.localEulerAngles = reticleRotation;
        reticleFar.transform.localEulerAngles = reticleRotation;

        //decay t for x&y
        if (tx != 0.0f){
            if (Mathf.Abs(tx)< 0.1f){
                tx = 0.0f;
            }else{
                tx -= Mathf.Clamp(tx, -0.1f, 0.1f);
            }
        }
        if (ty != 0.0f){
            if (Mathf.Abs(ty)< 0.1f){
                ty = 0.0f;
            }else{
                ty -= Mathf.Clamp(ty, -0.1f, 0.1f);
            }
        }
    }

    void FixedUpdate(){
        ix = gamepad.GetAxis(0);
        StateManager.SetFloat("ix", ix);
        iy = gamepad.GetAxis(1);
        StateManager.SetFloat("iy", iy);
        StateManager.SetBool("abutton", gamepad.GetButton(0));
        StateManager.SetBool("bbutton", gamepad.GetButton(1));
        StateManager.SetBool("sl", gamepad.GetButton(3));
        StateManager.SetBool("sr", gamepad.GetButton(4));

    }

    void OnDrawGizmos(){
        //draw bounding box of movement
        Gizmos.color = new Color(0.1059f, 0.8902f, 0.6941f);
        Gizmos.DrawWireCube(new Vector3(0, 0, 0), new Vector3(maxHorizontal+(minHorizontal*-1), maxVertical+(minVertical*-1), 0));
    }

    Projectile fireWeapons(GameObject prefab, Vector3 shipRotation) {
        GameObject bullet = Instantiate(prefab, blasterC.transform.position, Quaternion.identity);
        bullet.transform.localEulerAngles = shipRotation;
        return bullet.GetComponent<Projectile>();
    }

    public void FireLaser()
    {
        fireWeapons(BulletPrefab, transform.localEulerAngles);
    }

    public void OnHomingEnter(Collider collider) {
        HomingTarget = collider.gameObject;
        StateManager.SetTrigger("HomingEntered");
    }

    public void FireHoming() {
        if(HomingTarget && HomingTarget.GetType() == typeof(GameObject)) {
            //send homing shot to target
            Projectile homingShot = fireWeapons(HomingPrefab, transform.localEulerAngles);
            homingShot.setTarget(HomingTarget);
            HomingTarget = null;
        }
    }

    public void LockReticle() {
        if (HomingTarget && HomingTarget.GetType() == typeof(GameObject)) {
            //apply reticle to target
            GameObject TargetReticle = Instantiate(LockOnPrefab, HomingTarget.transform);
            TargetReticle.transform.parent = HomingTarget.transform;
            TargetReticle.SetActive(true);
        }
    }
}
