﻿public class Button
{
    public string name { get; protected set; }
    public bool pressed { get; set; }
    public Button(string _name)
    {
        name = _name;
    }
}
