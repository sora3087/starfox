﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [Header("Inherited from Projectile class")]
    public float velocity = 1.0f;
    public float despawnTime = 1.0f;
    public int damage = 1;
    public float maxHomingTurn = 20.0f;

    //timers
    protected float aliveTime = 0.0f;
    protected GameObject target;

    //states
    [SerializeField]
    bool isHoming = false;
    
    void FixedUpdate() {
        aliveTime += Time.deltaTime;
        if (aliveTime >= despawnTime) {
            Destroy(this.gameObject);
        }
        if (isHoming) {
            // transform.LookAt(target.transform);
            Vector3 relativePos = target.transform.position - transform.position;
	        Quaternion rotation = Quaternion.LookRotation(relativePos);
	        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, maxHomingTurn/360);
        }
        transform.position += transform.forward * velocity;
    }

    void OnTriggerEnter(Collider collider) {
        //collided with something
        Destroy(this.gameObject);
        Destroy(collider.gameObject);
    }

	public void setTarget(GameObject newTarget) {
        target = newTarget;
        isHoming = true;
	}

    public void setRotation(Vector3 angles) {
        transform.localEulerAngles = angles;
    }

    public void setVelocity(float v) {
        velocity = v;
    }
}
