﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {
	[Header("Inherited from Ship class")]
	public float health = 1;

	public void Hit(int damage){
		health -= damage;
		if(health <= 0){
			Destroy(gameObject);
		}
	}
}
