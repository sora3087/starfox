﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySine : Ship {
    public float maxLeft = -10.0f;
    public float maxRight = 10.0f;
    public float maxVertical = 5.0f;

    float h = 0;
    bool movingRight = true;
    float v = 0;
    bool vIncresing = true;

	// Use this for initialization
	void Start () {
		transform.position = new Vector3(Mathf.Lerp(maxLeft, maxRight, h), 0, transform.position.z);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(movingRight){
			h += 0.005f;
		} else {
			h -= 0.005f;
		}
		if(h >= 1.0f){
			h = 1.0f;
			movingRight = false;
		} else if(h <= 0.0f) {
			h = 0.0f;
			movingRight = true;
		}
		if(vIncresing){
			v += 0.02f;
		} else {
			v -= 0.02f;
		}
		if(v >= 1.0f){
			v = 1.0f;
			vIncresing = false;
		} else if(v <= 0.0f) {
			v = 0.0f;
			vIncresing = true;
		}
		transform.position = new Vector3(Mathf.Lerp(maxLeft, maxRight, h), Easing.easeInOutQuad(v)*maxVertical, transform.position.z);
	}
}
