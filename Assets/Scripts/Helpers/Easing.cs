﻿public static class Easing  {
    public static float easeInQuad(float t) {
        return t*t;
    }
    public static float easeOutQuad(float t) {
        return t*(2f-t);
    }
    public static float easeInOutQuad(float t) {
        return (t < 0.5f)? 2f*t*t : -1f+(4f-2f*t)*t;
    }
    public static float easeInCubic(float t) {
        return t*t*t;
    }
    public static float easeOutCubic(float t) {
        return (--t)*t*t+1f;
    }
    public static float easeInOutCubic(float t) {
        return (t < 0.5f)? 4f*t*t*t : (t-1f)*(2f*t-2f)*(2f*t-2f)+1f;
    }
    public static float easeInQuart(float t) {
        return t*t*t*t;
    }
    public static float easeOutQuart(float t) {
        return 1f-(--t)*t*t*t;
    }
    public static float easeInOutQuart(float t) {
        return (t < 0.5f) ? 8f*t*t*t*t : 1f-8f*(--t)*t*t*t;
    }
    public static float easeInQuint(float t) {
        return t*t*t*t*t;
    }
    public static float easeOutQuint(float t) {
        return 1f+(--t)*t*t*t*t;
    }
    public static float easeInOutQuint(float t) {
        return (t < 0.5f) ? 16f*t*t*t*t*t : 1f+16f*(--t)*t*t*t*t;
    }
}
